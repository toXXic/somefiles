```json
{
  "$schema": "http://json-schema.org/draft-04/schema#",
  "description": "Action search JSON-schema",
  "type": "object",
  "properties": {
    "hotel_id": {
      "type": "integer",
      "description": "Hotel's identifier"
    },
    "pay_type": {
      "type": "string",
      "enum": ["prepay", "postpay"],
      "description": "postpay - payment is made at the time of arrival at the hotel, prepay - payment is made before arrival, at the moment of booking"
    },
    "bed": {
      "type": "string",
      "enum": [
        "1 Double Bed",
        "2 Single Beds",
        "2 Queen Beds",
        "1 King Bed",
        "1 Double Bed or 2 Single Beds",
        "2 Double Beds",
        "1 Single Bed",
        "1 Queen Bed",
        "2 King Beds",
        "Bunk Beds",
        "3 Double Beds",
        "3 Single Beds",
        "Sofa Bed"
      ],
      "description": "Bed configuration information"
    },
    "breakfast": {
      "type": "boolean",
      "description": "Whether breakfasts are included (true - yes, false - no)."
    },
    "commission": {
      "type": "number",
      "description": "Amount of money per commission (= )."
    },
    "original_price": {
      "type": "number",
      "description": "Price of the hotel in local currency."
    },
    "price": {
      "type": "number",
      "description": "Price of the hotel in usd."
    },
    "price_per_night": {
      "type": "number",
      "description": "Price of the hotel in usd per night."
    },
    "tax": {
      "type": "number",
      "description": "Taxes."
    },
    "public": {
      "type": "boolean",
      "description": "Flag of the public price."
    },
    "refundable": {
      "type": "boolean",
      "description": "Money back flag"
    },
    "supplements": {
      "type": "array",
      "item": {
        "name": {
          "type": "string",
          "description": "Name of the supplements"
        },
        "price": {
          "type": "number",
          "description": "Amount of money to be paid additionally"
        }
      },
      "description": "Supplements information"
    },
    "supplier": {
      "type": "string",
      "description": "Name of the supplier"
    },
    "agent": {
      "type": "string",
      "enum": ["OTT", "PRL", "HPR"],
      "description": "Name of the agent"
    },
    "token": {
      "type": "string",
      "format": "uuid",
      "description": "Order token"
    },
    "group_name": {
      "type": "string",
      "description": "Имя группы отеля"
    },
    "meal_name": {
      "type": "string",
      "description": "Name of breakfast"
    },
    "room": {
      "type": "string",
      "description": "Name of room"
    },
    "room_desc": {
      "type": "string",
      "description": "Room description"
    },
    "revenue": {
      "type": "number",
      "description": "Доход от поставщика"
    },
    "size": {
      "type": "integer",
      "description": "Room size"
    },
    "size_ft": {
      "type": "number",
      "description": "Room size in feet"
    },
    "view_fac": {
      "type": "array",
      "item": {
        "id": {
          "type": "number",
          "description": "id of view"
        },
        "description": {
          "type": "string",
          "description": "description of view"
        }
      },
      "description": "Opportunities for a view from the window"
    },
    "view_full": {
      "type": "string",
      "description": "full description view from window"
    },
    "images": {
      "type": "array",
      "item": {
        "type": "string"
      },
      "description": "Url on hotel images"
    }
  },
  "required": [
    "hotel_id",
    "pay_type",
    "bed_configurations",
    "breakfast",
    "breakfast",
    "commission",
    "original_price",
    "price",
    "price_per_night",
    "tax",
    "public",
    "refundable",
    "supplements",
    "supplier",
    "agent",
    "token"
  ]
}
```
